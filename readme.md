### Introduction to Neural Networks

This is designed to be a fairly basic intro to how to instantiate, train, and fine-tune a neural network.

I'm not going over how they work conceptually in too much detail, I would recommend a quick youtube summary to bring you up to speed.

I'm keen to do something like this as I'd like to do some more advanced work on natural language or image classification, and NNs seem to be a more powerful tool here than a standard linear/ensembling model can do.

I'm just running this on the standard Titanic dataset, so no surprises there.

#### Model Compilation

There are two stages. First we create the model structure:
```python
model = keras.models.Sequential() # a sequential model is the simplest neural network

# layer one is the input layer, it should have as many neurones as features
model.add(keras.layers.Dense(8)) 

# not sure about this layer. We probably don't need a hidden layer for so simple a dataset, but it's nice to try
model.add(keras.layers.Dense(6))

# the last layer is the outcome layer. This is a classification task with two outcomes, so we're using a sigmoid activator
model.add(keras.layers.Dense(1, activation = 'sigmoid'))
```

And now we configure how it learns!
```python
model.compile(optimizer = 'SGD', # standard gradient descent
             loss = 'binary_crossentropy', # this is the default loss function for a binary outcome. A lower score is better.
             metrics = ['accuracy']) # the performance metric that we will report
# 'loss' is not the same as 'metrics' - the former is what we report, the latter is what the optimizer tries to decrease. So they're similar but not the same
```

Cool, let's train
```python
training = model.fit(x = np.array(scaled), # keras can return a history object so we can see info about the training process
                     y = np.array(y), # keras likes arrays, not pandas dataframes
                     verbose = 0,
                     validation_split = 0.2,
                     epochs = 100) # epochs is how many iterations we'll do on our data. it defaults to one. 
```
if you re-run this above cell multiple times, the performance metrics improve. The model remembers past trainings! of course, this likely means we'd be overfitting to the training data.

Notice that we're running 100 epochs. This seems like a lot. Worst case scenario we're overfitting, but even if we're not doing this many iterations could be a waste of time. Fortunately because we're doing cross validation we can see the training vs evaluation scores over time:

![Performance over time](model_accuracy.png)

Ok so we actually only need about 15-20 epochs. After this we see a slight decline, as we begin to overfit, followed by no change.
